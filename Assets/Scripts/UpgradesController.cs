﻿using System.Collections.Generic;
using UnityEngine;

public class UpgradesController : MonoBehaviour {

    private RectTransform _buttonTransform;

    private const int Speed = 2;

    private const int RotationDegrees = 90;

    private float _totalRotation = 0;

    private bool _rotateLeft = false;
    private bool _rotateRight = false;

    public Dictionary<string, int> EvoPoints = new Dictionary<string, int>();
    private Dictionary<string, int> _evoPointsCapacity = new Dictionary<string, int>(); 

    void Start()
    {
        _buttonTransform = GetComponent<RectTransform>();

        foreach (var branch in UpgradeBranches.Branches)
        {
            EvoPoints.Add(branch, 0);
            _evoPointsCapacity.Add(branch, 200);
        }
    }
   
    void Update()
    {
        if (Mathf.Abs(_totalRotation) < Mathf.Abs(RotationDegrees))
        {
            if (_rotateLeft)
            {
                _buttonTransform.RotateAround(_buttonTransform.position, Vector3.back, Speed);
                _totalRotation += Speed;
            }
            else if (_rotateRight)
            {
                _buttonTransform.RotateAround(_buttonTransform.position, Vector3.back, -Speed);
                _totalRotation += Speed;
            }
        }
        else
        {
            _rotateLeft = false;
            _rotateRight = false;
            _totalRotation = 0;
        }
    }

    public void RotateLeft ()
    {
        _rotateLeft = true;
    }

    public void RotateRight ()
    {
       _rotateRight = true;
    }

    public void AddEvoPoints(string branch, int amount)
    {
        if (EvoPoints[branch] + amount >= _evoPointsCapacity[branch])
            EvoPoints[branch] = _evoPointsCapacity[branch];
        else
            EvoPoints[branch] += amount;
    }
}