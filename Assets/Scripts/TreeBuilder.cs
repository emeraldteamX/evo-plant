﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public static class TreeBuilder
{
    public static event Action<ITreePart> TreePartDeath;
    public static event Action<ITreePart, float> ChangeProduction;

    private static void OnChangeProduction(ITreePart sender, float difference)
    {
        if (ChangeProduction != null)
            ChangeProduction(sender, difference);
    }

    #region Resource events and event handlers

    public static event Func<ResourceType, bool> ResourceSpawning;
    public static event Action<ResourceType> ResourcePickUp;
    public static event Action<ResourceType> ResourceDispose;

    private static bool OnResourceSpawning(ResourceType type)
    {
        if (ResourceSpawning != null)
            return ResourceSpawning(type);
        return false;
    }

    private static void OnResourceDispose(ResourceType type)
    {
        if (ResourceDispose != null)
            ResourceDispose(type);
    }

    private static void OnResourcePickUp(ResourceType type)
    {
        if (ResourcePickUp != null)
            ResourcePickUp(type);
    }

    private static void OnTreePartDeath(ITreePart treePart)
    {
        if (TreePartDeath != null)
            TreePartDeath(treePart);
    }

    #endregion

    public static event Action<ITreePart> IncreaseConsumption;
    public static event Action<ITreePart> IncreaseProduction;

    #region Rotation calculation

    /// <summary>
    /// Angle between two vectors. Both vectors rotates by basic angle
    /// </summary>
    /// <param name="firstStart">Start point of the first vector</param>
    /// <param name="firstEnd">End point of the first vector</param>
    /// <param name="secondStart">Start point of the second vector</param>
    /// <param name="secondEnd">End point of the second vector</param>
    /// <param name="basicAngle">Rotation angle</param>
    /// <returns>Angle between two vectors</returns>
    private static float Angle(Vector3 firstStart, Vector3 firstEnd, Vector3 secondStart, Vector3 secondEnd,
        float basicAngle)
    {
        var firstVector = new Vector2(firstEnd.x - firstStart.x, firstEnd.y - firstStart.y);
        var secondVector = new Vector2(secondEnd.x - secondStart.x, secondEnd.y - secondStart.y);

        return Vector2.Angle(FindRotatedVector(firstVector, basicAngle), FindRotatedVector(secondVector, basicAngle));
    }

    /// <summary>
    /// Rotates the vector by the angle
    /// </summary>
    /// <param name="vector">Vector to rotate</param>
    /// <param name="angle">Rotation angle</param>
    /// <returns>New Vector2</returns>
    private static Vector2 FindRotatedVector(Vector2 vector, float angle)
    {
        return new Vector2(vector.x * (float)Math.Cos(angle) - vector.y * (float)Math.Sin(angle),
            vector.y = vector.x * (float)Math.Sin(angle) + vector.y * (float)Math.Cos(angle));
    }

    #endregion

    private static List<PlanAroundPlantPart> _allPlans;
    private static GameObject _objToCreate;

    /// <summary>
    /// Get parent of new tree element part acording to clicked plan
    /// </summary>
    /// <param name="clickedPlan">Clicked plan</param>
    /// <param name="position">Plan position</param>
    /// <returns>Parent of tree element</returns>
    public static ITreePart FindParent(GameObject clickedPlan, out Position position)
    {
        position = Position.None;
        if (_allPlans == null)
            return null;

        foreach (var element in _allPlans)
            if (element.HaveParent(clickedPlan, ref position))
                return element.Parent;

        return null;
    }

    /// <summary>
    /// Is enough resource to build selected palnt part
    /// </summary>
    /// <param name="reserve">Global stock of resource</param>
    /// <param name="type">Type of plant part</param>
    /// <returns>Is enough resource to build selected palnt part</returns>
    public static bool CanBuild(Dictionary<ResourceType, float> reserve, TreeElementType type)
    {
        // todo: use type for calculating

        return reserve[ResourceType.Sun] >= ResourceGeneratorBase.SunPrice
               && reserve[ResourceType.Water] >= ResourceGeneratorBase.WaterPrice
               && reserve[ResourceType.Mineral] >= ResourceGeneratorBase.MineralPrice;
    }

    /// <summary>
    /// Decrease amount of resource in global stock acording to plant part type
    /// </summary>
    /// <param name="reserve">Global stock of resource</param>
    /// <param name="type">Type of plant part</param>
    public static void SpendResourceToBuilding(Dictionary<ResourceType, float> reserve, string type)
    {
        // todo: use type for calculating

        reserve[ResourceType.Sun] -= ResourceGeneratorBase.SunPrice;
        reserve[ResourceType.Water] -= ResourceGeneratorBase.WaterPrice;
        reserve[ResourceType.Mineral] -= ResourceGeneratorBase.MineralPrice;
    }

    /// <summary>
    /// Create plans objects around plant part
    /// </summary>
    /// <param name="placeHolder">Place holder game object</param>
    /// <param name="plan">Plan game object</param>
    /// <param name="selectedPlantPart">Selected plant part</param>
    /// <param name="objToCreate">Game object, that will be build</param>
    public static void CreatePlans(GameObject placeHolder, GameObject plan, ITreePart selectedPlantPart,
        GameObject objToCreate)
    {
        _objToCreate = objToCreate;

        if (_allPlans == null)
            _allPlans = new List<PlanAroundPlantPart>();
        _allPlans.Add(new PlanAroundPlantPart(placeHolder, selectedPlantPart, plan));
    }

    /// <summary>
    /// Destroy plans objects
    /// </summary>
    public static void DestroyPlans()
    {
        _objToCreate = null;
        if (_allPlans == null)
            return;

        foreach (var element in _allPlans)
            element.DestroyPlans();
        _allPlans = null;
    }

    /// <summary>
    /// Build new tree element object 
    /// </summary>
    /// <param name="controller">Game controller</param>
    /// <param name="selectedPlantPart">Selected plan</param>
    /// <param name="objToCreate">Game object that will be build</param>
    /// <param name="reserve">Global stock of resource</param>
    /// <returns>New tree element</returns>
    public static ITreePart BuildTreePart(GameController controller, GameObject selectedPlantPart,
        GameObject objToCreate, Dictionary<ResourceType, float> reserve)
    {
        // If not enough resource
        var objToCreateType = StringToEnum.Execute<TreeElementType>(objToCreate.tag);

        //foreach (TreeElementType treeElementType in Enum.GetValues(typeof(TreeElementType)))
        //{
        //    if (!StringEnum.GetStringValue(treeElementType).Equals(objToCreate.tag)) 
        //        continue;
        //    objToCreateType = treeElementType;
        //    break;
        //}

        if (!CanBuild(reserve, objToCreateType))
            return null;

        Position position;
        var plantParent = FindParent(selectedPlantPart, out position);

        // Add resource spawner if necessary
        GameObject resourceToSpawn = null;
        switch (objToCreateType)
        {
            case TreeElementType.Leaf:
                resourceToSpawn = controller.SunResource;
                break;
            case TreeElementType.Root:
                resourceToSpawn = controller.MineralsResource;
                break;
        }

        // Create new tree eleemnt
        var newTreePart = TreePartFactory.CreateTreePart(objToCreate, resourceToSpawn, plantParent);
        newTreePart.OwnPosition = position;
        var newGameObject = newTreePart.GameObjectInstance;

        newGameObject.transform.position = plantParent.GameObjectInstance.transform.position;
        newGameObject.transform.rotation = Quaternion.identity;

        // Rotate new tree element
        RotateObject(newGameObject, plantParent, position);

        // Add event listeners
        SetEventListeners(newTreePart);
        // Add joints
        SetJoints(newTreePart, plantParent, position);
        // Add new tree element to game controller list
        controller.Tree.Add(newTreePart);
        // Decrease amount of resource
        SpendResourceToBuilding(reserve, objToCreate.tag);

        controller.SetDefaultValues();

        if (IncreaseConsumption != null)
            IncreaseConsumption(newTreePart);

        if (IncreaseProduction != null)
            IncreaseProduction(newTreePart);

        return newTreePart;
    }

    /// <summary>
    /// Add joints to new tree element
    /// </summary>
    /// <param name="obj">New tree element</param>
    /// <param name="parent">Parent of new tree element</param>
    /// <param name="position">Position of new tree element</param>
    private static void SetJoints(ITreePart obj, ITreePart parent, Position position)
    {
        var hingeJoint = obj.GameObjectInstance.AddComponent<HingeJoint2D>();
        var specifications = ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(obj.GameObjectInstance.tag)];
        hingeJoint.anchor = new Vector2(0f, -obj.Height / 2f);
        var angle = 0f;
        if (parent.GameObjectInstance.GetComponent<HingeJoint2D>() != null)
            angle = parent.GameObjectInstance.GetComponent<HingeJoint2D>().jointAngle;
        hingeJoint.useLimits = true;
        hingeJoint.connectedBody = parent.GameObjectInstance.GetComponent<Rigidbody2D>();

        var joints = new[]
        {
            obj.GameObjectInstance.AddComponent<SpringJoint2D>(),
            obj.GameObjectInstance.AddComponent<SpringJoint2D>()
        };
        foreach (var joint in joints)
        {
            joint.connectedBody = parent.GameObjectInstance.GetComponent<Rigidbody2D>();
            joint.frequency = specifications.SpringInfo.Frequency;
        }

        var x = parent.Width / 2f;
        var y = parent.Height / 2f;

        var maxLimit = specifications.HingeInfo.Limits.max;
        var minLimit = specifications.HingeInfo.Limits.min;
        // Set joints anchors and hinge limits acording to position
        switch (position)
        {
            case Position.Top:
                joints[0].connectedAnchor = new Vector2(x, y);
                joints[1].connectedAnchor = new Vector2(-x, y);
                hingeJoint.connectedAnchor = new Vector2(0, y);
                break;
            case Position.Bottom:
                joints[0].connectedAnchor = new Vector2(x, -y);
                joints[1].connectedAnchor = new Vector2(-x, -y);
                hingeJoint.connectedAnchor = new Vector2(0, -y);
                maxLimit = 2.5f;
                minLimit = -2.5f;
                break;
            case Position.LeftBottom:
                hingeJoint.connectedAnchor = new Vector2(0, -y / 2);
                joints[1].connectedAnchor = new Vector2(0, -y);
                maxLimit = -120;
                minLimit = -60;
                break;
            case Position.RightBottom:
                hingeJoint.connectedAnchor = new Vector2(0, -y / 2);
                joints[1].connectedAnchor = new Vector2(0, -y);
                maxLimit = 120;
                minLimit = 60;
                break;
            case Position.LeftTop:
                hingeJoint.connectedAnchor = new Vector2(0, y / 2);
                joints[1].connectedAnchor = new Vector2(0, y);
                maxLimit = -120;
                minLimit = -60;
                break;
            case Position.RightTop:
                hingeJoint.connectedAnchor = new Vector2(0, y / 2);
                joints[1].connectedAnchor = new Vector2(0, y);
                maxLimit = 120;
                minLimit = 60;
                break;
        }
        hingeJoint.limits = new JointAngleLimits2D
        {
            max = maxLimit + angle,
            min = minLimit + angle
        };
    }

    /// <summary>
    /// Add event listeners to new tree element
    /// </summary>
    /// <param name="treePart">New tree element</param>
    private static void SetEventListeners(ITreePart treePart)
    {
        treePart.Death += OnTreePartDeath;
        treePart.ResourcePickUp += OnResourcePickUp;
        treePart.ResourceDispose += OnResourceDispose;
        treePart.ResourceSpawning += OnResourceSpawning;
        treePart.ChangeProduction += OnChangeProduction;
    }

    /// <summary>
    /// Additional class to keep plan game object and its position towards parent
    /// </summary>
    private class Plans
    {
        public GameObject Plan { get; set; }
        public Position PlanPosition { get; set; }
    }

    /// <summary>
    /// Rotate game object considering its parent position
    /// </summary>
    /// <param name="objToRotate">Object to rotate</param>
    /// <param name="parent">Plant parent</param>
    /// <param name="position">Position towards parent palnt</param>
    public static void RotateObject(GameObject objToRotate, ITreePart parent, Position position)
    {
        var k = (position == Position.LeftTop || position == Position.LeftBottom)
            ? 1f
            : -1f;
        var parentPosition = parent.GameObjectInstance.transform.position;
        var x = parentPosition.x;
        var y = parentPosition.y - parent.Height / 2.0f;
        objToRotate.transform.Rotate(Vector3.forward * k * Angle(new Vector3(x, y),
            parentPosition,
            new Vector3(x, y),
            objToRotate.transform.position,
            parent.GameObjectInstance.transform.rotation.eulerAngles.z));
    }

    /// <summary>
    /// Additional class to keep plans and appropriate tree element parent
    /// </summary>
    private class PlanAroundPlantPart
    {
        private List<Plans> _plans;
        public readonly ITreePart Parent;

        /// <summary>
        /// Create plans around parent tree element
        /// </summary>
        /// <param name="placeHolder">Place holder game object</param>
        /// <param name="parent">Parent tree element</param>
        /// <param name="plan">Plan game object</param>
        public PlanAroundPlantPart(Object placeHolder, ITreePart parent, Object plan)
        {
            Parent = parent;
            CreatePlans(placeHolder, plan, (float)Math.Pow(ElementsSpecifications.DecreaseScaleValue, parent.ScaleMultiplier + 1));
        }

        /// <summary>
        /// Create six plans game objects
        /// </summary>
        /// <param name="placeHolder">Place holder game object</param>
        /// <param name="plan">Plan game object</param>
        /// <param name="scaleCoeficient">Scale multiplier of new plant part</param>
        private void CreatePlans(Object placeHolder, Object plan, float scaleCoeficient)
        {
            // Left or right rotate
            var k = Parent.GameObjectInstance.transform.rotation.eulerAngles.z < 0
                ? 1.0
                : 0.0;
            // Available positions to build
            var availablePositions = new List<Position>()
            {
                Position.Top,
                Position.Bottom,
                Position.LeftBottom,
                Position.LeftTop,
                Position.RightBottom,
                Position.RightTop
            };
            // Remove positions to build if child occupies it 
            foreach (var child in Parent.Children)
            {
                availablePositions.Remove(child.OwnPosition);
            }

            // Create plans on available positions
            _plans = new List<Plans>();
            foreach (var position in availablePositions)
            {
                var newPlan = new Plans
                {
                    Plan = (GameObject)Object.Instantiate(plan, GetPlanCoordinate(position, Parent), Quaternion.identity),
                    PlanPosition = position
                };
                var planPosition = GetPlanCoordinate(newPlan.PlanPosition, Parent);
                // Create plan
                newPlan.Plan.GetComponent<PlanLogic>().Create(_objToCreate, placeHolder, Parent, planPosition);
                // Rotate place holder that attached to plan by necessary angle
                RotateObject(newPlan.Plan.GetComponent<PlanLogic>().PlaceHolder, Parent, position);
                // Change place holders collider size
                newPlan.Plan.GetComponent<PlanLogic>().PlaceHolder.GetComponent<BoxCollider2D>().size =
                    new Vector2(ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(_objToCreate.tag)].Width * scaleCoeficient,
                        ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(_objToCreate.tag)].Height * scaleCoeficient);
                // Rotate plan around parent
                RotateAroundPoint(Parent.GameObjectInstance.transform.position, newPlan.Plan,
                    (float)Math.Pow(-1.0, k) * Parent.GameObjectInstance.transform.rotation.eulerAngles.z);

                _plans.Add(newPlan);
            }
        }

        /// <summary>
        /// Get coordinates of plan acording to its position
        /// </summary>
        /// <param name="position">Plan position</param>
        /// <param name="parent">Plant parent</param>
        /// <returns>Plan coordinates</returns>
        private static Vector3 GetPlanCoordinate(Position position, ITreePart parent)
        {
            var parentCoordinate = parent.GameObjectInstance.transform.position;
            var height = parent.Height / 2.0f;
            var width = parent.Width / 2.0f;
            switch (position)
            {
                case Position.Top:
                    return new Vector3(parentCoordinate.x, height + parentCoordinate.y, parentCoordinate.z);
                case Position.Bottom:
                    return new Vector3(parentCoordinate.x, -height + parentCoordinate.y, parentCoordinate.z);
                case Position.LeftBottom:
                    return new Vector3(-width + parentCoordinate.x, parentCoordinate.y - height / 3.0f, parentCoordinate.z);
                case Position.RightBottom:
                    return new Vector3(width + parentCoordinate.x, parentCoordinate.y - height / 3.0f, parentCoordinate.z);
                case Position.LeftTop:
                    return new Vector3(-width + parentCoordinate.x, parentCoordinate.y + height / 3.0f, parentCoordinate.z);
                case Position.RightTop:
                    return new Vector3(width + parentCoordinate.x, parentCoordinate.y + height / 3.0f, parentCoordinate.z);
            }
            return Vector3.zero;
        }

        /// <summary>
        /// Destroy plans game objects
        /// </summary>
        public void DestroyPlans()
        {
            if (_plans == null)
                return;

            foreach (var element in _plans)
                Object.Destroy(element.Plan);
            _plans = null;
        }

        /// <summary>
        /// If current Parent is a parent of clicked plan
        /// </summary>
        /// <param name="clickedPlan">Clicked plan</param>
        /// <param name="position">Plan position</param>
        /// <returns>If current Parent is a parent of clicked plan</returns>
        public bool HaveParent(GameObject clickedPlan, ref Position position)
        {
            if (_plans == null)
                return false;
            foreach (var element in _plans)
            {
                if (!element.Plan.Equals(clickedPlan))
                    continue;
                position = element.PlanPosition;
                return true;
            }

            position = Position.None;
            return false;
        }

        /// <summary>
        /// Rotate object around point by the angle
        /// </summary>
        /// <param name="point">Rotation point</param>
        /// <param name="objToRotate">Object to rotate</param>
        /// <param name="angle">Rotation angle</param>
        private static void RotateAroundPoint(Vector3 point, GameObject objToRotate, float angle)
        {
            objToRotate.transform.RotateAround(point, Vector3.forward, angle);
        }
    }
}