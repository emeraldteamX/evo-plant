﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using SystemRandom = System.Random;

public class GameController : MonoBehaviour
{
    #region Unity gameobjects

    public GameObject Insect;

    public GameObject Plan;
    public GameObject PlaceHolder;

    public GameObject Body;
    public GameObject Root;
    public GameObject Leaf;

    public GUIText SunText;
    public GUIText WaterText;
    public GUIText MineralsText;
    public GUIText HelpText;

    public GameObject SunResource;
    public GameObject WaterResource;
    public GameObject MineralsResource;

    public Button[] BuildButtons;
    public Button[] ControlButtons;

    public GameObject WeatherController;

    public GameObject UpgradeButtons;
    public GameObject BuildButton;

    public Camera MainCamera;
    public Camera UpgradesCamera;

    #endregion

    #region Tree building attributes

    public Texture[] Images;

    private GameObject _objToCreate;

    private GameObject _selectedPlantPart;

    #endregion

    #region Economics

    /// <summary>
    /// Global production/consumption logic
    /// </summary>
    private class Economics
    {
        public MyDictionary Consumption { get; private set; }
        public MyDictionary Production { get; private set; }

        public Economics(Dictionary<ResourceType, float> consumption, Dictionary<ResourceType, float> production)
        {
            Consumption = new MyDictionary(consumption);
            Production = new MyDictionary(production);
            Consumption.Coeficient = ConsumptionCoeficient;
            Production.Coeficient = ProductionCoeficient;
        }
        
        private static float ConsumptionCoeficient(ResourceType type)
        {
            float solarCoeficient = LevelData.WeatherInfo.CurrentSolarActivity,
                waterCoeficient = 1f;
            if (LevelData.WeatherInfo.CurrentSolarActivity > 1)
            {
                solarCoeficient = 1f;
                waterCoeficient = LevelData.WeatherInfo.CurrentSolarActivity * 1.5f;
            }
            else if (LevelData.WeatherInfo.CurrentSolarActivity < 0.6f)
                solarCoeficient = 0.5f;

            return Time.deltaTime * solarCoeficient
                    * (type == ResourceType.Water
                    ? waterCoeficient
                    : 1f);
        }

        private static float ProductionCoeficient(ResourceType type)
        {
            float solarCoeficient;
            if (type == ResourceType.Sun)
                solarCoeficient = LevelData.WeatherInfo.CurrentSolarActivity;
            else
               solarCoeficient = _isDay
                ? 1f
                : 0.5f;
            return Time.deltaTime*solarCoeficient;
        }
    }
    
    /// <summary>
    /// Dictionary with additional logic
    /// </summary>
    private class MyDictionary
    {
        private readonly Dictionary<ResourceType, float> _dictionary;
        public Func<ResourceType, float> Coeficient;
        public MyDictionary(Dictionary<ResourceType, float> dictionary)
        {
            _dictionary = dictionary;
        }

        public float this[ResourceType type]
        {
            get
            {
                return _dictionary[type] * Coeficient(type);
            }
            set
            {
                _dictionary[type] = value;
            }
        }
    }

    #endregion

    public List<ITreePart> Tree { get; private set; }

    private float _timer;
    private static bool _isDay = true;

    public bool IsTreeEmpty
    {
        get
        {
            if (Tree == null)
                return true;

            foreach (var element in Tree)
                if (element != null)
                    return false;

            return true;
        }
    }

    private Economics GlobalEconomics { get; set; }

    public Dictionary<ResourceType, float> Reserve { get; private set; }
    public Dictionary<ResourceType, int> ReserveCapacity { get; private set; }
    public Dictionary<ResourceType, int> SpawnedObjects { get; private set; }

    #region Help text contents

    private void SetDefaultHelpText()
    {
        HelpText.text = "ClickOntoPlantToBuildPartOfPlant";
    }

    private void SetCantBuildHelpText()
    {
        HelpText.text = "NotEnoughResourceToBuild";
    }

    private void SetSelectButtonHelpText()
    {
        HelpText.text = "SelectNecessaryCommand"; //Resources.SelectNecessaryCommand;
    }

    #endregion

    #region Default options

    /// <summary>
    /// Set default values for ElementSpecifications, buttons, weather and some start values for attributes
    /// </summary>
    private void SetDefaultConstants()
    {
        // Set default ElementSpecifications values
        ElementsSpecifications.Elements = new Dictionary<TreeElementType, ElementsSpecifications.Element>();
        foreach (TreeElementType type in Enum.GetValues(typeof(TreeElementType)))
        {
            var rectTransform = GetType(type).GetComponent<RectTransform>();
            ElementsSpecifications.Elements.Add(type, new ElementsSpecifications.Element
            {
                Height = rectTransform.rect.height * rectTransform.localScale.y,
                Width = rectTransform.rect.width * rectTransform.localScale.x,
                Scale = GetType(type).transform.localScale,
                Price = new ElementsSpecifications.Price
                {
                    Minerals = 10f,
                    Suns = 20f,
                    Water = 20f
                },
                HingeInfo = new ElementsSpecifications.HingeJointParameters
                {
                    Limits = new JointAngleLimits2D
                    {
                        max = 30f,
                        min = -30f
                    }
                },
                SpringInfo = new ElementsSpecifications.SpringJointParameters
                {
                    Frequency = 10
                }
            });
        }

        // Weather default values
        GlobalWeather.DayDuration = 10f;
        GlobalWeather.NightDuration = 5f;
        GlobalWeather.SolarStep = 2f / (GlobalWeather.NightDuration + GlobalWeather.DayDuration) * Time.deltaTime;

        // Start value for _timer(middle of a day)
        _timer = GlobalWeather.DayDuration / 2f;

        // Enable rich text for text editing
        foreach (var button in BuildButtons)
        {
            button.GetComponentInChildren<Text>().supportRichText = true;
        }
        SetButtonsTextPrice();

        //Set default buttons position
        DefaultButtonsPosition.X = BuildButtons[0].transform.position.x;
        DefaultButtonsPosition.Y = BuildButtons[0].transform.position.y;

        //Set default camera
        MainCamera.enabled = true;
        UpgradesCamera.enabled = false;
        UpgradeButtons.SetActive(false);

        DeactivateButtons();
    }

    /// <summary>
    /// Get game object acording to type of plant part
    /// </summary>
    /// <param name="type">Type of tree element</param>
    /// <returns>Game object of tree element type</returns>
    public GameObject GetType(TreeElementType type)
    {
        switch (type)
        {
            case TreeElementType.Body:
                return Body;
            case TreeElementType.Leaf:
                return Leaf;
            case TreeElementType.Root:
                return Root;
        }
        return null;
    }

    /// <summary>
    /// Set default values of some game controller attributes and objects
    /// </summary>
    public void SetDefaultValues()
    {
        SetDefaultHelpText();
        _objToCreate = null;
        if (_selectedPlantPart != null)
            _selectedPlantPart.GetComponent<SpriteRenderer>().color = Color.white;
        _selectedPlantPart = null;
        TreeBuilder.DestroyPlans();
        DeactivateButtons();
    }

    /// <summary>
    /// Deactivate buttons and set their default positions
    /// </summary>
    private void DeactivateButtons()
    {
        foreach (var button in BuildButtons)
        {
            button.gameObject.SetActive(false);
        }
        foreach (var button in ControlButtons)
        {
            button.gameObject.SetActive(false);
        }
    }

    #endregion

    public LevelData LevelData { get; private set; }

    public ITreePart FindTreePart(GameObject gameObject)
    {
        foreach (var element in Tree)
            if (gameObject.Equals(element.GameObjectInstance))
                return element;
        return null;
    }

    private void Start()
    {
        SetBackground();

        SetDefaultConstants();
        // todo: No hardcode

        Reserve = new Dictionary<ResourceType, float>
        {
            {ResourceType.Sun, 2000f},
            {ResourceType.Water, 2000f},
            {ResourceType.Mineral, 1000f}
        };

        ReserveCapacity = new Dictionary<ResourceType, int>
        {
            {ResourceType.Sun, 100},
            {ResourceType.Water, 100},
            {ResourceType.Mineral, 100}
        };

        SpawnedObjects = new Dictionary<ResourceType, int>
        {
            {ResourceType.Sun, 0},
            {ResourceType.Water, 0},
            {ResourceType.Mineral, 0}
        };

        GlobalEconomics = new Economics(
            // Consumption
            new Dictionary<ResourceType, float>
            {
                {ResourceType.Sun, 0},
                {ResourceType.Water, 0},
                {ResourceType.Mineral, 0}
            },
            // Production
            new Dictionary<ResourceType, float>
            {
                {ResourceType.Sun, 0},
                {ResourceType.Water, 0},
                {ResourceType.Mineral, 0}
            }
        );

        Tree = new List<ITreePart>();
        StartBuildings();

        TreeBuilder.TreePartDeath += DestroyElement;
        TreeBuilder.ResourcePickUp += AddResource;
        TreeBuilder.ResourceDispose += OnResourceDispose;
        TreeBuilder.ResourceSpawning += OnResourceSpawning;
        TreeBuilder.ChangeProduction += OnChangeProduction;

        SetDefaultValues();
        //updateText();
        LevelData.WeatherInfo = new WeatherInfo
        {
            CurrentSolarActivity = 1f
        };
        LevelData = new LevelData
        {
            RandomQuests = new List<IQuest>
            {
                QuestFactory.CreateRandomQuest()
            },
            QuestsToWin = new List<IQuest>()
        };
        foreach (var quest in LevelData.RandomQuests)
        {
            Debug.Log(quest.WinCondition);
        }

        WeatherController.GetComponent<WeatherController>().WeatherInfo = LevelData.WeatherInfo;

        TreeBuilder.IncreaseConsumption += OnIncreaseConsumption;
        TreeBuilder.IncreaseProduction += OnIncreaseProduction;
    }

    /// <summary>
    /// Set a random background image 
    /// </summary>
    private static void SetBackground()
    {
        var dir = new DirectoryInfo(Application.dataPath + "/Textures/Backgrounds");
        var info = dir.GetFiles("*.jpg");

        var random = new SystemRandom();

        var fileData = File.ReadAllBytes(info[random.Next(info.Length)].FullName);

        var texture2D = new Texture2D(2, 2);
        texture2D.LoadImage(fileData);

        var sp = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 250);

        GameObject.Find(Global.BackgroundName).GetComponent<SpriteRenderer>().sprite = sp;
    }

    private void Update()
    {
        PassiveIncome();
        PassiveLoss();
        CapacityOverflowCheck();
        CheckForQuests();
        UpdateText();
        ChangeSolarActivity();
        ChangeBackgroundColor();
        GainExperience();
    }

    /// <summary>
    /// Change times of day and appropriate solar activity value
    /// </summary>
    private void ChangeSolarActivity()
    {
        var weather = LevelData.WeatherInfo.Weather;
        LevelData.WeatherInfo.ChangeDayTime(_isDay, _timer);
        if (_isDay)
        {
            if (_timer > GlobalWeather.DayDuration)
            {
                Debug.Log("Night");
                _isDay = false;
                _timer = 0f;
                LevelData.WeatherInfo.ChangeSolarActivity(weather, _isDay);
            }
        }
        else
        {
            if (_timer > GlobalWeather.NightDuration)
            {
                Debug.Log("Day");
                _isDay = true;
                _timer = 0f;
                LevelData.WeatherInfo.ChangeSolarActivity(ChangeWeather(weather), _isDay);
                ChangeCloudiness();
            }
        }
        _timer += Time.deltaTime;
    }

    /// <summary>
    /// Start or stop clouds creating
    /// </summary>
    private void ChangeCloudiness()
    {
        var cloudiness = WeatherController.GetComponent<WeatherController>().IsCloudiness;
        WeatherController.GetComponent<WeatherController>().IsCloudiness = Random.value < 0.5f
            ? !cloudiness
            : cloudiness;
        Debug.Log(cloudiness ? "start cloudiness" : "end cloudiness");
    }

    /// <summary>
    /// Set new weather with some probability
    /// </summary>
    /// <param name="currentWeather">Current weather</param>
    /// <returns>New weather</returns>
    private static WeatherType ChangeWeather(WeatherType currentWeather)
    {
        switch (currentWeather)
        {
            case WeatherType.None:
            case WeatherType.Rain:
                currentWeather = Random.Range(0, 2) == 0
                    ? WeatherType.Rain
                    : WeatherType.None;
                return currentWeather;
        }
        return WeatherType.None;
    }

    /// <summary>
    /// Change background color acording to solar activity
    /// </summary>
    private static void ChangeBackgroundColor()
    {
        var color = Color.white * LevelData.WeatherInfo.CurrentSolarActivity;
        color.a = 1f;
        GameObject.Find(Global.BackgroundName).GetComponent<SpriteRenderer>().color = color;
    }

	void OnGUI()
    {
        if (Event.current.type.Equals(EventType.mouseDown))
            OnMouseClick();
    }

    /// <summary>
    /// Add build buttons for select
    /// </summary>
    public void SelectForBuilding()
    {
        SetSelectButtonHelpText();
        _objToCreate = null;

        BuildMenu();

        CancelButton(true);
        RemoveButton(false);
    }

    public bool OnUpgrade(UpgradesContainer container)
    {
        if (!CanUpgrade(container.UpgradeCost))
        {
            Debug.Log(CanUpgrade(container.UpgradeCost));
            return false;
        }
        TakeResources(container.UpgradeCost);
        foreach (var element in Tree)
        {
            if (element.GameObjectInstance.tag == container.TreePartName)
            {
                foreach (ResourceType key in Enum.GetValues(typeof (ResourceType)))
                {
                    element.Production[key] *= container.ProductionChange[key];
                    element.Consumption[key] *= container.ConsumptionChange[key];
                }
            }

            var newSprite = (Sprite) Resources.Load(container.NewSpriteName);
            if (newSprite != null)
            {
                element.GameObjectInstance.GetComponent<SpriteRenderer>().sprite = newSprite;
                SetTexture(container.TreePartName, newSprite);
            }

            if (container.NewUpgradeComponent == null) continue;
            switch (container.NewUpgradeComponent)
            {
                case UpgradeComponents.Damaging:
                    if (element.GameObjectInstance.GetComponent<Damaging>() == null)
                        element.GameObjectInstance.AddComponent<Damaging>();
                    break;
            }

        }
        return true;
    }

    private bool CanUpgrade(IDictionary<ResourceType, float> cost)
    {
        return cost[ResourceType.Sun] <= Reserve[ResourceType.Sun] &&
               cost[ResourceType.Mineral] <= Reserve[ResourceType.Mineral] &&
               cost[ResourceType.Water]<= Reserve[ResourceType.Water];
    }

    private void TakeResources(IDictionary<ResourceType, float> resourcesToTake)
    {
        foreach (ResourceType resource in Enum.GetValues(typeof(ResourceType)))
            Reserve[resource] -= resourcesToTake[resource];
    }

    private void SetTexture(string treePartName, Sprite newSprite)
    {
        switch (StringToEnum.Execute<TreeElementType>(treePartName))
        {
            case TreeElementType.Leaf:
                Leaf.GetComponent<SpriteRenderer>().sprite = newSprite;
                break;
            case TreeElementType.Root:
                Root.GetComponent<SpriteRenderer>().sprite = newSprite;
                break;
            case TreeElementType.Body:
                Body.GetComponent<SpriteRenderer>().sprite = newSprite;
                break;
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;

    }

    public void PlayGame()
    {
        Time.timeScale = 1f;

    }

    public void FasterPlayGame()
    {
        Time.timeScale = 1.5f;

    }

    public void SwitchCamera()
    {
        MainCamera.enabled = !MainCamera.enabled;
        UpgradesCamera.enabled = !UpgradesCamera.enabled;

        SunText.enabled = !SunText.enabled;

        BuildButton.SetActive(MainCamera.enabled);
        UpgradeButtons.SetActive(UpgradesCamera.enabled);
    }

    /// <summary>
    /// Build first tree body
    /// </summary>
    private void StartBuildings()
    {
        _objToCreate = Body;
        _objToCreate.transform.position = new Vector3(0.0f, -3.0f, 5.0f);
        _objToCreate.transform.rotation = Quaternion.identity;

        var element = TreePartFactory.CreateTreePart(resource: null, treeElementObject: _objToCreate, parent: null);
        element.Death += DestroyElement;
        element.ResourcePickUp += AddResource;
        element.ResourceDispose += OnResourceDispose;
        element.ResourceSpawning += OnResourceSpawning;
        element.ChangeProduction += OnChangeProduction;

        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
            ReserveCapacity[type] += element.Capacity[type];

        Tree.Add(element);
        _objToCreate = null;
    }

    /// <summary>
    /// Destroy _selectedPlantPart element
    /// </summary>
    public void DestroySelectedElement()
    {
        if (_selectedPlantPart == null)
            return;

        FindTreePart(_selectedPlantPart).Die();
        SetDefaultValues();

        //foreach (var element in Tree)
        //    if (_selectedPlantPart.Equals(element.GameObjectInstance))
        //    {
        //        element.Die();
        //        SetDefaultValues();
        //        return;
        //    }
    }

    /// <summary>
    /// Destroy sender element
    /// </summary>
    /// <param name="sender">Element to destroy</param>
    public void DestroyElement(ITreePart sender)
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
        {
            Reserve[type] -= Reserve[type] * sender.Capacity[type] / ReserveCapacity[type];
            ReserveCapacity[type] -= sender.Capacity[type];
            GlobalEconomics.Consumption[type] -= sender.Consumption[type];
            GlobalEconomics.Production[type] -= sender.Production[type];
        }

        foreach (var element in Tree)
        {
            if (!sender.Equals(element))
                continue;
            Tree.Remove(element);
            return;
        }
    }

    /// <summary>
    /// Add resource to global stock
    /// </summary>
    /// <param name="type">Type of resource to add</param>
    public void AddResource(ResourceType type)
    {
        if (!Reserve.ContainsKey(type))
            return;

        Reserve[type]++;
    }

    private void GainExperience()
    {
        foreach (var treePart in Tree)
        {
            //todo: Gain experience
        }
    }

    /// <summary>
    /// Passive production of resources by time
    /// </summary>
    private void PassiveIncome()
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
            Reserve[type] += GlobalEconomics.Production[type];
    }

    private void CheckForQuests()
    {
        for (var i = 0; i < LevelData.RandomQuests.Count; i++)
        {
            if (!LevelData.RandomQuests[i].IsDone)
                continue;

            LevelData.RandomQuests[i].GetReward();
            LevelData.RandomQuests.RemoveAt(i--);
        }

        for (var i = 0; i < LevelData.QuestsToWin.Count; i++)
        {
            if (!LevelData.QuestsToWin[i].IsDone)
                continue;

            LevelData.QuestsToWin[i].GetReward();
            LevelData.QuestsToWin.RemoveAt(i--);
        }
    }

    private void CapacityOverflowCheck()
    {
        foreach (ResourceType type in Enum.GetValues(typeof (ResourceType)))
            Reserve[type] = Math.Min(ReserveCapacity[type], Reserve[type]);
    }
    /// <summary>
    /// Use passive consumption of every resource to feed tree from reserve or
    /// to make damage if there's not enough resources.
    /// </summary>
    /// <param name="totalConsumption"></param>
    /// <param name="type"></param>
    private void FeedTree(float totalConsumption, ResourceType type)
    {
        if (!Reserve.ContainsKey(type))
            return;

        if (totalConsumption <= Reserve[type])
        {
            Reserve[type] -= totalConsumption;
        }
        else
        {
            Reserve[type] = 0;
            var damage = (1 - Reserve[type] / totalConsumption) * 0.5f; // todo: 0.5f need to be an external constant

            foreach (var element in Tree)
            {
                if (Math.Abs(element.Consumption[type]) > 0)
                    continue;
                element.Damage(damage);
                break;
            }
        }
    }

    /// <summary>
    /// Passive consumption of resources by time
    /// </summary>
    private void PassiveLoss()
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
        {
            FeedTree(GlobalEconomics.Consumption[type], type);
        }
    }

    /// <summary>
    /// On mouse click action
    /// </summary>
    private void OnMouseClick()
    {
        if (UpgradesCamera.enabled) return;
        if (!Input.GetMouseButtonDown(0))
            return;

        var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider == null)
            return;

        var hitObject = hit.collider.gameObject;
        switch (hitObject.layer)
        {
            // Hit object is a plan
            case LayerLevel.Plan:
                DeactivateButtons();

                var newBuilding = TreeBuilder.BuildTreePart(this, hitObject, _objToCreate, Reserve);
                newBuilding.IsActive = false;

                foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
                    ReserveCapacity[type] += newBuilding.Capacity[type];

                var speed = (ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(newBuilding.GameObjectInstance.tag)].Scale - newBuilding.GameObjectInstance.transform.localScale) / 5f;
                var multiplier = (float)Math.Pow(ElementsSpecifications.DecreaseScaleValue, newBuilding.ScaleMultiplier);

                StartCoroutine(Grow(speed, newBuilding, multiplier));
                break;
            // Hit object is some picked resource
            case LayerLevel.Resource:
                hit.collider.GetComponent<ResourceController>().Collect();
                break;
            // Hit object is a plant part
            case LayerLevel.Plant:
                if(StringToEnum.Execute<TreeElementType>(hitObject.tag) == TreeElementType.Root)
                    UpgradeButton(true);
                CancelButton(true);
                RemoveButton(true);
                // Old plant part
                if (_selectedPlantPart != null)
                    _selectedPlantPart.GetComponent<SpriteRenderer>().color = Color.white;
                // New plant part
                _selectedPlantPart = hit.collider.gameObject;
                _selectedPlantPart.GetComponent<SpriteRenderer>().color -= new Color(0.4f, 0.3f, 0.1f, 0f);
                break;
        }
    } 

    /// <summary>
    /// Change local scale from zero to necessary value
    /// </summary>
    /// <param name="speed">Growing speed</param>
    /// <param name="obj">Object that growing</param>
    /// <param name="scaleMultiplier">Parameter, that help define final scale value</param>
    /// <returns>IEnumerator for coroutine</returns>
    private static IEnumerator Grow(Vector3 speed, ITreePart obj, float scaleMultiplier)
    {
        obj.GameObjectInstance.GetComponent<Rigidbody2D>().isKinematic = false;
        obj.GameObjectInstance.transform.localScale = Vector3.zero;
        var currentScale = obj.GameObjectInstance.transform.localScale;
        var finalScale = ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(obj.GameObjectInstance.tag)].Scale * scaleMultiplier;
        // If obj not deleted
        while (obj.GameObjectInstance != null)
        {
            // Compare coordinates of two points
            if ((currentScale.x < finalScale.x) && (currentScale.y < finalScale.y) && (currentScale.z < finalScale.z))
            {
                obj.GameObjectInstance.transform.localScale += speed * Time.deltaTime;
                currentScale = obj.GameObjectInstance.transform.localScale;
                yield return null;
            }
            else
            {
                obj.IsActive = true;
                yield break;
            }
        }
    }

    /// <summary>
    /// Is enought resource to build selected plant part
    /// </summary>
    /// <param name="buttonType">Button type, that define plant part to build</param>
    /// <returns>Is enought resource to build selected plant part</returns>
    public bool IsEnoughtResource(ButtonType buttonType)
    {
        switch (buttonType)
        {
            case ButtonType.ButtonBody:
                return TreeBuilder.CanBuild(Reserve, TreeElementType.Body);
            case ButtonType.ButtonRoot:
                return TreeBuilder.CanBuild(Reserve, TreeElementType.Root);
            case ButtonType.ButtonLeaf:
                return TreeBuilder.CanBuild(Reserve, TreeElementType.Leaf);
        }
        return false;
    }

    /// <summary>
    /// Menu to select plant type for building
    /// </summary>
    private void BuildMenu()
    {
        foreach (var button in BuildButtons)
        {
            button.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Button to cancel previous actions
    /// </summary>
    private void CancelButton(bool isActive)
    {
        ControlButtons[0].gameObject.SetActive(isActive);
    }

    /// <summary>
    /// Button to delete selected plant part
    /// </summary>
    private void RemoveButton(bool isActive)
    {
        ControlButtons[1].gameObject.SetActive(isActive);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="isActive"></param>
    private void UpgradeButton(bool isActive)
    {
        ControlButtons[2].gameObject.SetActive(isActive);
    }

    public void UpgradeSelectedElement()
    {
        if (_selectedPlantPart == null)
            return;

        FindTreePart(_selectedPlantPart).Capacity[ResourceType.Mineral] += 300;     // todo: creat capacity constants
        ReserveCapacity[ResourceType.Mineral] += 300;

        var dir = new DirectoryInfo(Application.dataPath + "/Textures");

        var fileData = File.ReadAllBytes(dir.GetFiles("Жук.png")[0].FullName);

        var texture2D = new Texture2D(2, 2);
        texture2D.LoadImage(fileData);

        var sp = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 250);

        _selectedPlantPart.GetComponent<SpriteRenderer>().sprite = sp;
    }
    /// <summary>
    /// Add plans to select object location on the screen, if current amount of resource is enough
    /// </summary>
    /// <param name="obj">New plant part game object</param>
    public void SetObjectForCreate(GameObject obj)
    {
        // if current amount of resource is not enough
        if (!TreeBuilder.CanBuild(Reserve, StringToEnum.Execute<TreeElementType>(obj.tag)))
        {
            SetCantBuildHelpText();
            return;
        }
        _objToCreate = obj;

        // Add plans
        foreach (var element in Tree)
            if (element.IsActive && StringToEnum.Execute<TreeElementType>(element.GameObjectInstance.tag) == TreeElementType.Body)
                TreeBuilder.CreatePlans(PlaceHolder, Plan, element, _objToCreate);

        DeactivateButtons();
        CancelButton(true);
    }
    protected bool OnResourceSpawning(ResourceType type)
    {
        if (SpawnedObjects[type] >= Spawn.MaxNumber)
            return false;

        SpawnedObjects[type]++;
        return true;
    }
    protected void OnResourceDispose(ResourceType type)
    {
        SpawnedObjects[type]--;
        if (SpawnedObjects[type] < 0)
            SpawnedObjects[type] = 0;
    }

    protected void OnIncreaseConsumption(ITreePart sender)
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
            GlobalEconomics.Consumption[type] += sender.Consumption[type];
    }

    protected void OnIncreaseProduction(ITreePart sender)
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
            GlobalEconomics.Production[type] += sender.Production[type];
    }

    protected void OnChangeProduction(ITreePart sender, float difference)
    {
        foreach (ResourceType type in Enum.GetValues(typeof(ResourceType)))
            GlobalEconomics.Production[type] -= sender.Production[type] * difference / 100;
    }

    /// <summary>
    /// Update text with amounts of current resources
    /// </summary>
    private void UpdateText()
    {
        SunText.text = string.Format("Sun: {0:0} / {1:0}", Reserve[ResourceType.Sun], ReserveCapacity[ResourceType.Sun]);
        MineralsText.text = string.Format("Minerals: {0:0} / {1:0}", Reserve[ResourceType.Mineral], ReserveCapacity[ResourceType.Mineral]);
        WaterText.text = string.Format("Water: {0:0} / {1:0}", Reserve[ResourceType.Water], ReserveCapacity[ResourceType.Water]);
    }

    /// <summary>
    /// Add necessary plant part price to build buttons
    /// </summary>
    public void SetButtonsTextPrice()
    {
        foreach (TreeElementType type in Enum.GetValues(typeof(TreeElementType)))
        {
            var element = ElementsSpecifications.Elements[type];
            GetButton(type).GetComponentInChildren<Text>().text = string.Format("<color=orange>{0}</color> <color=green>{1}</color> <color=darkblue>{2}</color>",
                element.Price.Suns,
                element.Price.Minerals,
                element.Price.Water);
        }
    }

    /// <summary>
    /// Get button acording to tree element type
    /// </summary>
    /// <param name="type">Type of tree element</param>
    /// <returns>Button with tree element type sprite</returns>
    private Button GetButton(TreeElementType type)
    {
        switch (type)
        {
            case TreeElementType.Body:
                return BuildButtons[0];
            case TreeElementType.Root:
                return BuildButtons[1];
            case TreeElementType.Leaf:
                return BuildButtons[2];
        }
        return null;
    }
 }
