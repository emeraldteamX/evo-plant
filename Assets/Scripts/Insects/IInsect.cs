﻿using UnityEngine;
using System;

public interface IInsect
{
    event Action<IInsect> Death;

    float Hunger { get; set; }
    float Agression { get; set; }
    float Health { get; set; }
    float Damage { get; set; }
    GameObject ThisInsect { get; set; }

    void OnMove();

    void SetTarget(Collider2D other);
}
