﻿using UnityEngine;
using System.Collections;
using System;
using Object = UnityEngine.Object;

public abstract class InsectBase : IInsect 
{
    public event Action<IInsect> Death;

    public float Hunger { get; set; }
    public float Agression { get; set; }
    public float Health { get; set; }
    public float Damage { get; set; }
    public GameObject ThisInsect { get; set; }

    protected GameObject Target;

    protected Rigidbody2D _rigidbody;

    protected CircleCollider2D _radar;

    protected GameController _gameController;

    protected InsectBehaivour InsectBehaviour;

    protected InsectBase(GameObject newInsectObj)
    {
        ThisInsect = (GameObject)Object.Instantiate(newInsectObj, new Vector3(UnityEngine.Random.value * 2, UnityEngine.Random.value * 3, 5), Quaternion.identity);
        ThisInsect.AddComponent<InsectBehaivour>();

        InsectBehaviour = ThisInsect.GetComponent<InsectBehaivour>();
        InsectBehaviour.Move += OnMove;
        InsectBehaviour.FindTarget += SetTarget;

        _rigidbody = ThisInsect.GetComponent<Rigidbody2D>();
    }

    public virtual void OnMove()
    {

    }

    public virtual void SetTarget(Collider2D other)
    {

    }
}
