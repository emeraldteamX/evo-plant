﻿using UnityEngine;

public class Insect : MonoBehaviour
{

    private float _hunger;
    private float _agression;

    private bool _isScanning;
    private bool _isAttacking;
    private bool _isEating;

    private GameObject _target;

    private Vector2 _randomForce;
    private Vector3 _landingPosition;

    private Rigidbody2D _rigidbody;

    private CircleCollider2D _radar;

    // Use this for initialization
    void Start()
    {
        _isAttacking = false;
        _isEating = false;
        _isScanning = true;

        _hunger = Random.value * 70f + 30f;
        _agression = Random.value;
        _rigidbody = GetComponent<Rigidbody2D>();

        _radar = gameObject.AddComponent<CircleCollider2D>();
        _radar.radius = 3f;
        _radar.isTrigger = true;

        _landingPosition = new Vector3(0, 2f, 0);

        _rigidbody.velocity = new Vector2(Random.value - 0.5f, Random.value - 0.5f);

        InvokeRepeating("generateForce", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {

        if (_rigidbody.position.y > 10 || GetComponent<Rigidbody2D>().position.y < -10 || GetComponent<Rigidbody2D>().position.x < -15 || GetComponent<Rigidbody2D>().position.x > 15)
            Destroy(_rigidbody.gameObject);

        if (_rigidbody.velocity.x < 0 && GetComponent<Rigidbody2D>().transform.rotation.eulerAngles.y == 0)
            _rigidbody.transform.Rotate(Vector3.up * 180f);

        if (_rigidbody.velocity.x > 0 && (int)(GetComponent<Rigidbody2D>().transform.rotation.eulerAngles.y - 180f) == 0)
            _rigidbody.transform.Rotate(Vector3.down * 180f);

        if (_isScanning)
        {
            _rigidbody.AddForce(_randomForce);
        }
        else
            if (_isAttacking)
            {
                if (_target == null)
                {
                    _isAttacking = false;
                    _isScanning = true;
                    return;
                }

                if (isClose(gameObject.transform.position, _target.transform.position + _landingPosition, 0.1f))
                {
                    _rigidbody.velocity = new Vector2(_target.transform.position.x - gameObject.transform.position.x, _target.transform.position.y - gameObject.transform.position.y);
                }

                if (isClose(gameObject.transform.position, _target.transform.position, 0.01f))
                {
                    _rigidbody.velocity = Vector2.zero;
                    _isAttacking = false;
                    _isEating = true;
                }
            }
            else
                if(_isEating)
                {
                    Destroy(gameObject, 2f);
                }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (_isAttacking)
            return;
        // todo: attack the plant or not
        if (other.gameObject.layer == LayerLevel.Plant)
        {
            Debug.Log("Yay!");
            if (_agression * _hunger > 0f && _hunger > 0)
            {
                Debug.Log("Die bitch!!!");

                Destroy(_radar);

                _isAttacking = true;
                _isScanning = false;

                _target = other.gameObject;

                Vector2 SpeedToTarget = new Vector2(_target.transform.position.x - gameObject.transform.position.x, _target.transform.position.y - gameObject.transform.position.y + _landingPosition.y);

                _rigidbody.velocity = SpeedToTarget.normalized;
            }
        }
    }

    private void generateForce()
    {
        _randomForce = new Vector2(3 * (0.5f - Random.value), 1.5f * (0.5f - Random.value));
    }

    private bool isClose(Vector3 Position, Vector3 Target, float Radius)
    {
        return (Position - Target).magnitude < Radius;
    }
}
