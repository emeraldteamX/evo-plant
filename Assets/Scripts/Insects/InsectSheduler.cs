﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InsectSheduler : MonoBehaviour {

    public GameObject FlyingInsect;
    public GameObject GroundInsect;
    public GameObject UndergroundInsect;

    public List<IInsect> _activeInsects;

	// Use this for initialization
	void Start () {
        _activeInsects = new List<IInsect>();

        InvokeRepeating("SpawnInsect", Insects.WaitTime, Insects.SpawnTime);
	}

    private void SpawnInsect()
    {
        var chance = Random.value;
        IInsect newInsect;

        if (chance < 0.3)
        {
            newInsect = InsectFactory.CreateInsect(FlyingInsect);
        }
        else
            if (chance < 0.7)
            {
                newInsect = InsectFactory.CreateInsect(FlyingInsect);
            }
            else
            {
                newInsect = InsectFactory.CreateInsect(FlyingInsect);
            }

        _activeInsects.Add(newInsect);
    }
}
