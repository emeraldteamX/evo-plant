﻿using UnityEngine;
using System.Collections;
using System;


public class UndergroundInsect : IInsect 
{
    public event Action<IInsect> Death;
    public float Hunger { get; set; }
    public float Agression { get; set; }
    public float Health { get; set; }
    public float Damage { get; set; }
    public GameObject ThisInsect { get; set; }

    public enum State
    {
        isScanning,
        isAttacking,
        isLanding,
        isEating,
        isGettingAway
    };

    public State Status;

    public GameObject Target { get; set; }

    public Vector3 LandingPosition { get; set; }

    public Rigidbody2D _rigidbody { get; set; }

    public Vector2 RandomForce { get; set; }

    private GameController GameController;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    public void OnMove()
    {

    }
    public void SetTarget(Collider2D other)
    {

    }
}
