﻿using UnityEngine;
using System.Collections;
using System;

public static class InsectFactory 
{
	static public IInsect CreateInsect(GameObject insectObject)
    {
        foreach (var name in Enum.GetNames(typeof(InsectType)))
            if (name == insectObject.tag)
                return CreateInsect(insectObject, Type.GetType(insectObject.tag));
        return null;
    }

    static private IInsect CreateInsect(GameObject insectObject, Type insectType)
    {
        var newInsect = (IInsect)Activator.CreateInstance(insectType, args: new object[] { insectObject });
        newInsect.Hunger = 100;
        newInsect.Agression = 1;        // todo: fix constants
        newInsect.Health = 100f;
        newInsect.Damage = 0;

        return newInsect;
    }
}

