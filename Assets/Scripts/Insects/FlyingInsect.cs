﻿using UnityEngine;
using System.Collections;
using System;


public class FlyingInsect : InsectBase
{
    private enum State
    {
        isScanning,
        isAttacking,
        isLanding,
        isEating,
        isGettingAway
    };

    private State _status;

    private Vector3 _landingPosition;

    public FlyingInsect(GameObject newInsectObj)
        : base(newInsectObj)
    {
        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        _gameController = gameControllerObject.GetComponent<GameController>();

        Debug.Log(ThisInsect);

        _radar = ThisInsect.AddComponent<CircleCollider2D>();
        _radar.radius = 3f;
        _radar.isTrigger = true;

        _status = State.isScanning;

        _landingPosition = new Vector3(0, 2f * UnityEngine.Random.value, 0);
    }

    void Start()
    {

    }

    public override void OnMove()
    {
        base.OnMove();

        if (_rigidbody.position.y > 10 || _rigidbody.position.y < -10 || _rigidbody.position.x < -15 || _rigidbody.position.x > 15)
        {
            GameObject.Destroy(ThisInsect);
            return;
        }

        if (_rigidbody.velocity.x < 0 && _rigidbody.transform.rotation.eulerAngles.y == 0)
            _rigidbody.transform.Rotate(Vector3.up * 180f);

        if (_rigidbody.velocity.x > 0 && (int)(_rigidbody.transform.rotation.eulerAngles.y - 180f) == 0)
            _rigidbody.transform.Rotate(Vector3.down * 180f);

        switch (_status)
        {
            case State.isScanning:
                _rigidbody.AddForce(InsectBehaviour.RandomForce);
                break;
            case State.isAttacking:
                if (Target == null)
                {
                    _status = State.isScanning;
                    return;
                }
                if (IsClose(ThisInsect.transform.position, Target.transform.position + _landingPosition, 0.1f))
                {
                    _rigidbody.velocity = new Vector2(Target.transform.position.x - ThisInsect.transform.position.x, Target.transform.position.y - ThisInsect.transform.position.y);
                    _status = State.isLanding;
                }
                break;
            case State.isLanding:
                if (IsClose(ThisInsect.transform.position, Target.transform.position, 0.01f))
                {
                    _rigidbody.velocity = Vector2.zero;
                    _status = State.isEating;
                }
                break;
            case State.isEating:
                ThisInsect.GetComponent<PolygonCollider2D>().isTrigger = true;

                if (Target == null)
                {
                    _status = State.isScanning;
                    return;
                }

                if (Hunger <= 0)
                {
                    _status = State.isGettingAway;
                    ThisInsect.GetComponent<PolygonCollider2D>().isTrigger = false;
                    return;
                }

                var damage = Insects.MaxDamage * UnityEngine.Random.value * Time.deltaTime;
                _gameController.FindTreePart(Target).Damage(damage, true);
                Hunger -= damage;
                Debug.Log("Yammi!");
                break;
            case State.isGettingAway:
                _rigidbody.AddForce(InsectBehaviour.RandomForce);
                break;
        }
    }

    public override void SetTarget(Collider2D other)
    {
        Debug.Log(other.gameObject.layer + " ! " + _status);
        if (other.gameObject.layer != LayerLevel.Plant || _status != State.isScanning) 
            return;

        Debug.Log("Yay!");
        if (!(Agression*Hunger >= 0f) || !(Hunger >= 0)) 
            return;

        Debug.Log("Die bitch!!!");

        GameObject.Destroy(_radar);

        _status = State.isAttacking;

        Target = other.gameObject;

        var SpeedToTarget = new Vector2(Target.transform.position.x - ThisInsect.transform.position.x, Target.transform.position.y - ThisInsect.transform.position.y + _landingPosition.y);

        _rigidbody.velocity = SpeedToTarget.normalized;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (_status == State.isScanning) 
            return;

        _rigidbody.velocity = Vector2.zero;
        _status = State.isScanning;
    }

    private static bool IsClose(Vector3 Position, Vector3 Target, float Radius)
    {
        return (Position - Target).magnitude < Radius;
    }    
}
