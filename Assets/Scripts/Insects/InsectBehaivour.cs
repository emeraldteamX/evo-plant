﻿using UnityEngine;
using System.Collections;
using System;

public class InsectBehaivour : MonoBehaviour 
{
    public event Action Move;

    public event Action<Collider2D> FindTarget;

    public Vector2 RandomForce;

    void Start()
    {
        InvokeRepeating("GenerateForce", 0f, 1f);
    }

    void Update()
    {
        if (Move != null)
            Move();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Touch it!");

        if (FindTarget != null)
            FindTarget(other);
    }

    private void GenerateForce()
    {
        RandomForce = new Vector2(3 * (0.5f - UnityEngine.Random.value), 1.5f * (0.5f - UnityEngine.Random.value));
    }
}
