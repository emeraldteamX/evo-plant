﻿using UnityEngine;

public class PlanLogic : MonoBehaviour
{
    public GameObject ObjToCreate { get; set; }
    public GameObject Parent { get; set; }

    public GameObject PlaceHolder { get; set; }

    public bool IsDestroy { get; set; }

    public bool SoilCollision { get; set; }

    /// <summary>
    /// Initialize plans attributes
    /// </summary>
    /// <param name="objToCreate">Object to create in game</param>
    /// <param name="placeHolder">Place holder game object</param>
    /// <param name="parent">Plant parent</param>
    /// <param name="position">Place holder position</param>
    public void Create(GameObject objToCreate, Object placeHolder, ITreePart parent, Vector3 position)
    {
        ObjToCreate = objToCreate;
        Parent = parent.GameObjectInstance;
        // Create place holder
        PlaceHolder = (GameObject)Object.Instantiate(placeHolder, position, Quaternion.identity);
        PlaceHolder.GetComponent<PlaceHolderLogic>().PlanParent = gameObject;
        PlaceHolder.GetComponent<PlaceHolderLogic>().PlantParent = parent;
    }

    public void Update()
    {
        if (IsDestroy)
        {
            Destroy(gameObject);
        }

        if (ObjToCreate.tag.Equals(TreeElementType.Root))
        {
            // If root in air
            if (!SoilCollision)
                Destroy(gameObject);
        }
        // If tree element in soil
        else if (SoilCollision)
            Destroy(gameObject);

        GetComponent<CircleCollider2D>().radius = 0.5f;
        var s = GetComponent<SpriteRenderer>();
        s.color = new Color(1f, 1f, 1f, 1f);
    }

    private void OnDestroy()
    {
        Destroy(PlaceHolder);
    }
}
