﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class TreePartFactory
{
    /// <summary>
    /// Create tree part
    /// </summary>
    /// <param name="resource">Resource which will be spawned by element</param>
    /// <param name="treeElementObject">Object of tree element to create</param>
    /// <param name="parent">TreePartBase parent of current object</param>
    /// <returns></returns>
    static public ITreePart CreateTreePart(GameObject treeElementObject, GameObject resource, ITreePart parent)
    {
        foreach (var name in Enum.GetNames(typeof(TreeElementType)))
            if (name == treeElementObject.tag)
                return CreateTreePart(Type.GetType(treeElementObject.tag), treeElementObject, resource, parent);
        
        return null;
    }

    /// <summary>
    /// Create tree part
    /// </summary>
    /// <param name="resource">Resource which will be spawned by element</param>
    /// <param name="treeElementType"></param>
    /// <param name="treeElementObject">Object of tree element to create</param>
    /// <param name="parent">TreePartBase parent of current object</param>
    /// <returns></returns>
    static private ITreePart CreateTreePart(Type treeElementType, GameObject treeElementObject, GameObject resource, ITreePart parent)
    {
        var producton = new Dictionary<ResourceType, float>
        {
            { ResourceType.Sun, 0 },
            { ResourceType.Mineral, 0 },
            { ResourceType.Water, 0 }
        };
        var consumption = new Dictionary<ResourceType, float>
        {
            { ResourceType.Sun, 0 },
            { ResourceType.Mineral, 0 },
            { ResourceType.Water, 0 }
        };
        var capacity = new Dictionary<ResourceType, int>      // todo: change capacity of different tree parts
        {
            { ResourceType.Sun, StringToEnum.Execute<TreeElementType>(treeElementObject.tag) == TreeElementType.Leaf ? Capacity.Sun : 0 },
            { ResourceType.Mineral, StringToEnum.Execute<TreeElementType>(treeElementObject.tag) == TreeElementType.Root ? Capacity.Mineral : 0 },
            { ResourceType.Water, StringToEnum.Execute<TreeElementType>(treeElementObject.tag) == TreeElementType.Body ? Capacity.Water : 0 }
        };

        if (resource != null)
        {
            switch (StringToEnum.Execute<TreeElementType>(treeElementObject.tag))
            {
                case TreeElementType.Leaf: producton[ResourceType.Sun] = ProductionValues.SunProduction;
                    consumption[ResourceType.Mineral] = ProductionValues.MineralConsumption;
                    consumption[ResourceType.Water] = ProductionValues.WaterConsumption;
                    break;
                case TreeElementType.Root: producton[ResourceType.Water] = ProductionValues.WaterProduction;
                    producton[ResourceType.Mineral] = ProductionValues.MineralProduction;
                    consumption[ResourceType.Sun] = ProductionValues.SunConsumption;
                    break;
            }
        }
        var newTreePart = (ITreePart)Activator.CreateInstance(treeElementType, resource, treeElementObject, parent);
        newTreePart.Production = producton;
        newTreePart.Consumption = consumption;
        newTreePart.Capacity = capacity;
        newTreePart.Health = 100f;
        return newTreePart;
    }
}