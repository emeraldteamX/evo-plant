﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonOnClick : MonoBehaviour
{
    public GameController Controller;
    private Button _button;
    private int _heightPosition;
    private ButtonType _buttonType;

    void Awake()
    {
        _button = GetComponent<Button>();
        _buttonType = StringToEnum.Execute<ButtonType>(tag);
        if (_button.GetComponentInChildren<Text>() != null)
            _button.GetComponentInChildren<Text>().GetComponent<RectTransform>().sizeDelta =
                new Vector2(Math.Max(Math.Min(Screen.width * 0.1f, Screen.height * 0.15f),
                100), Math.Max(Screen.height * 0.1f, 30));
        var side = Math.Min(Screen.width * 0.1f, Screen.height * 0.15f);
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(side, side);
        switch (_buttonType)
        {
            case ButtonType.ButtonUpgrade:
            case ButtonType.ButtonBody:
                _heightPosition = 5;
                break;
            case ButtonType.ButtonRoot:
                _heightPosition = 4;
                break;
            case ButtonType.ButtonLeaf:
                _heightPosition = 3;
                break;
            case ButtonType.ButtonCancel:
                _heightPosition = 2;
                break;
            case ButtonType.ButtonRemove:
                _heightPosition = 1;
                break;
        }
    }

    void OnDisable()
    {
        _button.gameObject.transform.position = new Vector3(DefaultButtonsPosition.X, DefaultButtonsPosition.Y);
    }

    void Update()
    {
        switch (_buttonType)
        {
            case ButtonType.ButtonBody:
            case ButtonType.ButtonRoot:
            case ButtonType.ButtonLeaf:

                _button.interactable = Controller.IsEnoughtResource(StringToEnum.Execute<ButtonType>(tag));

                if (gameObject.transform.position.y > Screen.height * 0.15f * _heightPosition)
                {
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x,
                        gameObject.transform.position.y - Screen.height * 0.15f * _heightPosition / (10.0f * _heightPosition), 0.0f);
                }
                break;
        }
    }

    void OnEnable()
    {
        switch (_buttonType)
        {
            case ButtonType.ButtonUpgrade:
            case ButtonType.ButtonCancel:
            case ButtonType.ButtonRemove:
                gameObject.transform.position = new Vector3(DefaultButtonsPosition.X, Screen.height * 0.15f * _heightPosition, 0.0f);
                break;
        }
    }

    public void OnClick()
    {
        switch (_buttonType)
        {
            case ButtonType.ButtonUpgrade:
                Controller.UpgradeSelectedElement();
                break;
            case ButtonType.ButtonBody:
                Controller.SetObjectForCreate(Controller.Body);
                break;
            case ButtonType.ButtonRoot:
                Controller.SetObjectForCreate(Controller.Root);
                break;
            case ButtonType.ButtonLeaf:
                Controller.SetObjectForCreate(Controller.Leaf);
                break;
            case ButtonType.ButtonCancel:
                Controller.SetDefaultValues();
                break;
            case ButtonType.ButtonRemove:
                Controller.DestroySelectedElement();
                break;
        }
    }

}
