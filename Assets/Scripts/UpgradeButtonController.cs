﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButtonController : MonoBehaviour
{
    public string UpgradeName;

    public Button ParentButton;
    public Button ChildButton;

    private UpgradesContainer _container = new UpgradesContainer();

    private int _upgradeLevel = 0; 
    
    private const int MaxLevel = 1;

    private Button _button;

    public bool IsMaxLevel()
    {
        return _upgradeLevel == MaxLevel;
    }

    void Start()
    {
        _button = GetComponent<Button>();
        _container = Deserialize(UpgradeName);

        if (ParentButton != null)
        {
            _button.interactable = false;
            _button.GetComponentInChildren<Text>().enabled = false;
        }

        _button.onClick.AddListener(() => AddLevel());

        _button.GetComponentInChildren<Text>().text =
            string.Format("<color=orange>{0}</color> <color=green>{1}</color> <color=darkblue>{2}</color>",
                    _container.UpgradeCost[ResourceType.Sun],
                    _container.UpgradeCost[ResourceType.Mineral],
                    _container.UpgradeCost[ResourceType.Water]);
    }

    public UpgradesContainer Deserialize(string upgradeName)
    {
        var serializer = new XmlSerializer(typeof(UpgradesContainer), new XmlRootAttribute() { ElementName = "upgrade" });
        var sr = File.OpenRead("Assets/TextPresets/"+ upgradeName + ".txt");

        return (UpgradesContainer)serializer.Deserialize(sr);
    }

    public void Serialize()
    {
        var serializer = new XmlSerializer(typeof (UpgradesContainer), new XmlRootAttribute() {ElementName = "upgrade"});
        var sr = File.Create("Assets/TextPresets/UpgradePreset.txt");

        serializer.Serialize(sr, _container);

        sr.Close();
    }

    void AddLevel()
    {
        if (!GameObject.Find(Global.GameControllerName).GetComponent<GameController>().OnUpgrade(_container))
            return;

        _upgradeLevel++;
        if (!IsMaxLevel()) return;

        _button.interactable = false;
        _button.GetComponentInChildren<Text>().enabled = false;

        if (ChildButton == null) return;
        ChildButton.interactable = true;
        ChildButton.GetComponentInChildren<Text>().enabled = true;
    }
}
