﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

class BuildNumberOfTreePartQuest : IQuest
{
    private GameController _controller;

    private readonly string _winCondition;
    private readonly string _reward;

    public Dictionary<TreeElementType, float> TreePartsToWin { get; set; }
    public Dictionary<ResourceType, float> RewardResources { get; set; }

    public BuildNumberOfTreePartQuest()
    {
        _controller = GameObject.Find(Global.GameControllerName).GetComponent<GameController>();

        var random = new Random();

        TreePartsToWin = new Dictionary<TreeElementType, float>();
        RewardResources = new Dictionary<ResourceType, float>();

        var treeElementTypes = new List<TreeElementType>
        {
            TreeElementType.Leaf,
            TreeElementType.Root,
            TreeElementType.Body
        };

        for (var i = 0; i < random.Next(1, 3); i++)
        {
            var index = random.Next(treeElementTypes.Count);
            TreePartsToWin.Add(treeElementTypes[index], random.Next(1, 6));
            treeElementTypes.RemoveAt(index);
        }

        var resourceTypes = new List<ResourceType>
        {
            ResourceType.Water,
            ResourceType.Mineral,
            ResourceType.Sun
        };

        for (var i = 0; i < random.Next(1, 4); i++)
        {
            var index = random.Next(resourceTypes.Count);
            RewardResources.Add(resourceTypes[index], random.Next(1, 4) * 500);
            resourceTypes.RemoveAt(index);
        }

        foreach (var treePartToWin in TreePartsToWin)
        {
            _winCondition += string.Format("{0} {1} {2} {3} >= {4}\n", treePartToWin.Key, "amount", "must", "be", treePartToWin.Value);
        }
        foreach (var resource in RewardResources)
        {
            _reward += string.Format("{0} {1} {2} {3} {4}\n", "you", "will", "get", resource.Value, resource.Key);
        }
    }

    public bool IsDone
    {
        get
        {
            var result = true;
            foreach (var treePartToWin in TreePartsToWin)
            {
                var count = 0;
                foreach (var treePart in _controller.Tree)
                {
                    if (StringToEnum.Execute<TreeElementType>(treePart.GameObjectInstance.tag) == treePartToWin.Key)
                        count++;
                }
                if (count < treePartToWin.Value)
                    result = false;
            }
            return result;
        }
        set
        {

        }
    }

    public string WinCondition
    {
        get { return _winCondition; }
        set { }
    }

    public string Reward
    {
        get { return _reward; }
        set { }
    }

    /// <summary>
    /// Get reward for completing quest
    /// </summary>
    public void GetReward()
    {
        if (!IsDone)
            return;

        foreach (var resource in RewardResources)
        {
            _controller.Reserve[resource.Key] += resource.Value;
        }
    }
}
