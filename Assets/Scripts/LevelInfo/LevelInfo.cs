﻿using System;
using System.Collections.Generic;

[Serializable]
public class LevelData
{
    public List<IQuest> QuestsToWin { get; set; }

    public List<IQuest> RandomQuests { get; set; }

    public static WeatherInfo WeatherInfo { get; set; }

    public Dictionary<string, float> StartReserve { get; set; }

    // todo: Add info about starting upgrades and available upgrades
}