﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

class GetCurrentAmountOfResourceQuest : IQuest
{

    private GameController _controller;

    private readonly string _winCondition;
    private readonly string _reward;

    public Dictionary<ResourceType, float> ResourcesToWin { get; set; }
    public Dictionary<ResourceType, float> RewardResources { get; set; }

    public GetCurrentAmountOfResourceQuest()
    {
        _controller = GameObject.Find(Global.GameControllerName).GetComponent<GameController>();

        var random = new Random();

        ResourcesToWin = new Dictionary<ResourceType, float>();
        RewardResources = new Dictionary<ResourceType, float>();

        var resourceTypes = new List<ResourceType>
        {
            ResourceType.Water,
            ResourceType.Mineral,
            ResourceType.Sun
        };

        for (var i = 0; i < random.Next(1, 3); i++)
        {
            var index = random.Next(resourceTypes.Count);
            ResourcesToWin.Add(resourceTypes[index], random.Next(1, 4) * 500);
            resourceTypes.RemoveAt(index);
        }

        resourceTypes = new List<ResourceType>
        {
            ResourceType.Water,
            ResourceType.Mineral,
            ResourceType.Sun
        };

        for (var i = 0; i < random.Next(1, 4); i++)
        {
            var index = random.Next(resourceTypes.Count);
            RewardResources.Add(resourceTypes[index], random.Next(1, 4) * 500);
            resourceTypes.RemoveAt(index);
        }

        foreach (var resource in ResourcesToWin)
        {
            _winCondition += string.Format("{0} {1} {2} {3} >= {4}\n", resource.Key, "amount", "must", "be", resource.Value);
        }
        foreach (var resource in RewardResources)
        {
            _reward += string.Format("{0} {1} {2} {3} {4}\n", "you", "will", "get", resource.Value, resource.Key);
        }
    }

    public bool IsDone
    {
        get
        {
            var result = true;
            foreach (var resource in ResourcesToWin)
            {
                if (_controller.Reserve[resource.Key] < resource.Value)
                    result = false;
            }
            return result;
        }
        set
        {

        }
    }

    public string WinCondition
    {
        get { return _winCondition; }
        set { }
    }

    public string Reward
    {
        get { return _reward; }
        set { }
    }

    /// <summary>
    /// Get reward for completing quest
    /// </summary>
    public void GetReward()
    {
        if (!IsDone)
            return;

        foreach (var resource in RewardResources)
        {
            _controller.Reserve[resource.Key] += resource.Value;
        }
    }
}
