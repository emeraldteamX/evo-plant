﻿using System;
using Random = System.Random;

public static class QuestFactory
{
    private static readonly Type[] Types = {
        typeof (GetCurrentAmountOfResourceQuest),
        typeof(BuildNumberOfTreePartQuest)
    };

    public static IQuest CreateRandomQuest()
    {
        var random = new Random();

        return (IQuest)Activator.CreateInstance(Types[random.Next(Types.Length)]);
    }
}