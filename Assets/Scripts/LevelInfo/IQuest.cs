﻿public interface IQuest
{
    bool IsDone { get; set; }

    string WinCondition { get; set; }


    string Reward { get; set; }

    void GetReward();
}