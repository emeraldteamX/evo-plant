﻿using UnityEngine;

public class WeatherInfo
{
    public float CurrentSolarActivity { get; set; }

    public float MaxSolarActivity { get; set; }

    public WeatherType Weather { get; set; }

    public int NumberOfClouds { get; set; }

    public WeatherInfo()
    {
        Weather = WeatherType.None;
        NumberOfClouds = 0;
    }

    /// <summary>
    /// Change solar activity acording to new weather and day time
    /// </summary>
    /// <param name="weather">New weather</param>
    /// <param name="isDay">current day time</param>
    public void ChangeSolarActivity(WeatherType weather, bool isDay)
    {
        Debug.Log(weather == WeatherType.None ? "Weather: none" : "Weather: rain");
        Weather = weather;
        if (!isDay)
        {
            MaxSolarActivity = 0.3f;
            return;
        }

        switch (Weather)
        {
            case WeatherType.None:
                MaxSolarActivity = 1f;
                break;
            case WeatherType.Rain:
                MaxSolarActivity = 0.8f;
                break;
        }
    }

    /// <summary>
    /// Change day time
    /// </summary>
    /// <param name="isDay">Is current day time - day</param>
    /// <param name="timer">Current time</param>
    public void ChangeDayTime(bool isDay, float timer)
    {
        //not middle day part
        if ((isDay && timer < GlobalWeather.DayDuration / 3f) || (!isDay && timer > 2 * GlobalWeather.NightDuration / 3f))
        {
            if (CurrentSolarActivity < CloudFactor())
                CurrentSolarActivity += GlobalWeather.SolarStep;
            else
                CurrentSolarActivity = CloudFactor();
        }
        //not middle night part
        else if ((isDay && timer > 2 * GlobalWeather.DayDuration / 3f) || (!isDay && timer < GlobalWeather.NightDuration / 3f))
        {
            if (CurrentSolarActivity > 0.3f)
                CurrentSolarActivity -= GlobalWeather.SolarStep;
            else
                CurrentSolarActivity = 0.3f;
        }
    }

    /// <summary>
    /// Show max solar activity acording to number of clouds
    /// </summary>
    /// <returns>Current max solar activity</returns>
    private float CloudFactor()
    {
        return MaxSolarActivity - 0.01f * NumberOfClouds > 0.3f
            ? MaxSolarActivity - 0.01f * NumberOfClouds
            : 0.3f;
    }
}
