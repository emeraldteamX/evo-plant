﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public abstract class TreePartBase : ITreePart
{
    public event Action<ITreePart> Death;

    public event Action<ITreePart, float> ChangeProduction;

    #region Tree part properties

    protected float Defence;
    public Position OwnPosition { get; set; }
    public float Height { get; set; }
    public float Width { get; set; }
    public bool IsActive { get; set; }
    public bool IsRecovering { get; set; }

    public float Health { get; set; }
    public float Exprerience { get; set; }
    public Dictionary<ResourceType, float> Consumption { get; set; }
    public Dictionary<ResourceType, float> Production { get; set; }
    public Dictionary<ResourceType, int> Capacity { get; set; }
    public ITreePart Parent { get; set; }

    public static float SunPrice { get; protected set; }

    public static float WaterPrice { get; protected set; }

    public static float MineralPrice { get; protected set; }

    public List<ITreePart> Children { get; set; }

    public int ScaleMultiplier { get; set; }

    public GameObject GameObjectInstance
    {
        get
        {
            return TreeElement;
        }
    }

    #endregion

    #region Resource events and event handlers

    public event Func<ResourceType, bool> ResourceSpawning;
    public event Action<ResourceType> ResourcePickUp;
    public event Action<ResourceType> ResourceDispose;

    protected bool OnResourceSpawning(ResourceType type)
    {
        return ResourceSpawning != null && ResourceSpawning(type);
    }

    protected void OnResourceDispose(ResourceType type)
    {
        if (ResourceDispose != null)
            ResourceDispose(type);
    }

    protected void OnResourcePickUp(ResourceType type)
    {
        if (ResourcePickUp != null)
            ResourcePickUp(type);
    }

    #endregion

    protected readonly GameObject TreeElement;

    protected TreePartBase(GameObject resource, GameObject gameObject, ITreePart parent)
    {
        Consumption = new Dictionary<ResourceType, float>();
        Production = new Dictionary<ResourceType, float>();
        Capacity = new Dictionary<ResourceType, int>();

        var element = ElementsSpecifications.Elements[StringToEnum.Execute<TreeElementType>(gameObject.tag)];

        SunPrice = element.Price.Suns;
        WaterPrice = element.Price.Water;
        MineralPrice = element.Price.Minerals;

        TreeElement = (GameObject)Object.Instantiate(gameObject, gameObject.transform.position, gameObject.transform.rotation);

        Height = TreeElement.GetComponent<RectTransform>().rect.height;
        Width = TreeElement.GetComponent<RectTransform>().rect.width;

        IsActive = true;
        Parent = parent;

        if (parent == null)
        {
            ScaleMultiplier = 0;
            return;
        }
        ScaleMultiplier = parent.ScaleMultiplier + 1;
        TreeElement.transform.localScale *= (float)Math.Pow(ElementsSpecifications.DecreaseScaleValue, ScaleMultiplier);

        if (parent.Children == null)
            return;

        parent.Children.Add(this);
    }

    public void Recover()
    {

    }

    public void Damage(float damage, bool isFromOutside = false)
    {
        var prevHealth = Health;
        Health -= damage * (isFromOutside ? 1 - Defence : 1);

        if (ChangeProduction != null)
            ChangeProduction(this, prevHealth - Health);

        GameObjectInstance.GetComponent<SpriteRenderer>().color -= new Color(0f, 0.001f, 0.001f, 0f);

        if (Health > 0)
            return;

        Health = 0f;
        Die();
    }

    public virtual void Die()
    {
        if (Death != null)
            Death(this);

        if (Parent == null)
            return;

        foreach (var child in Parent.Children)
            if (child.GameObjectInstance.Equals(GameObjectInstance))
            {
                Parent.Children.Remove(child);
                break;
            }
        Object.Destroy(TreeElement);
    }

    public void GenerateExperience()
    {

    }
}