﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface ITreePart
{
    /// <summary>
    /// Fires when tree part dies
    /// </summary>
    event Action<ITreePart> Death;

    /// <summary>
    /// Fires on production value chenge
    /// </summary>
    event Action<ITreePart, float> ChangeProduction; 

    event Func<ResourceType, bool> ResourceSpawning;

    event Action<ResourceType> ResourcePickUp;

    event Action<ResourceType> ResourceDispose;

    #region Tree part properties

    Position OwnPosition { get; set; }

    float Height { get; set; }

    float Width { get; set; }

    bool IsActive { get; set; }

    bool IsRecovering { get; set; }

    float Health { get; set; }

    float Exprerience { get; set; }

    Dictionary<ResourceType, float> Consumption { get; set; }

    Dictionary<ResourceType, float> Production { get; set; }

    Dictionary<ResourceType, int> Capacity { get; set; }

    ITreePart Parent { get; set; }

    List<ITreePart> Children { get; set; }

    int ScaleMultiplier { get; set; }

    GameObject GameObjectInstance { get; }

    #endregion

    void Recover();

    void Damage(float damage, bool isFromOutside = false);

    /// <summary>
    /// Destroy element, its children and their pictures on screen
    /// </summary>
    void Die();

    /// <summary>
    /// Generate experience of this tree part branch
    /// </summary>
    void GenerateExperience();
}
