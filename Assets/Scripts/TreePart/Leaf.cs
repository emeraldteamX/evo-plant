﻿using UnityEngine;

class Leaf : ResourceGeneratorBase
{
    public Leaf(GameObject resource, GameObject gameObject, ITreePart parent)
        : base(resource, gameObject, parent)
    {
    }
}
