﻿using System.Collections.Generic;
using UnityEngine;

class Root : ResourceGeneratorBase
{
    public Root(GameObject resource, GameObject gameObject, ITreePart parent)
        : base(resource, gameObject, parent)
    {
        Children = new List<ITreePart>();
    }

    /// <summary>
    /// Destroy element, its children and their pictures on screen
    /// </summary>
    override public void Die()
    {
        foreach (var child in Children)
            child.Die();

        base.Die();
    }
}
