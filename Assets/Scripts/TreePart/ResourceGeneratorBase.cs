﻿using UnityEngine;

public abstract class ResourceGeneratorBase : TreePartBase
{
    public ResourceGeneratorBase(GameObject resource, GameObject gameObject, ITreePart parent) 
        : base(resource, gameObject, parent)
    {
        if (resource == null)
            return;

        var component = TreeElement.AddComponent<ResourceSpawner>();
        component.ResourceToSpawn = resource;
        component.Spawner = TreeElement;

        component.Spawning += OnResourceSpawning;
        component.ResourcePickUp += OnResourcePickUp;
        component.ResourceDispose += OnResourceDispose;
    }
}
