﻿using System.Collections.Generic;
using UnityEngine;

public abstract class TreePartWithChildren : TreePartBase
{
    public TreePartWithChildren(GameObject resource, GameObject gameObject, ITreePart parent)
        : base(resource, gameObject, parent)
    {
        Children = new List<ITreePart>();
    }

    /// <summary>
    /// Destroy element, its children and their pictures on screen
    /// </summary>
    override public void Die()
    {
        foreach (var child in Children)
            child.Die();

        base.Die();
    }
}