﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BuildOnClick : MonoBehaviour
{
    public GameController Controller;

    private void Start()
    {
        var side = Math.Min(Screen.width / 10f, Screen.height / 10f);
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(side, side);
    }

    public void OnClick()
    {
        if (Controller.IsTreeEmpty)
            return;
        TreeBuilder.DestroyPlans();
        Controller.SelectForBuilding();
    }
}
