﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

public class UpgradesContainer
{
    public string TreePartName = "Leaf";

    public string NewSpriteName = "Assets/Textures/";

    public string NewUpgradeComponent = string.Empty;

    public float EvoPointsCost = 1f; 

    public SerializableDictionary<ResourceType, float> UpgradeCost = new SerializableDictionary<ResourceType, float>()
    {
        {ResourceType.Sun, 20f},
        {ResourceType.Mineral, 20f},
        {ResourceType.Water, 20f}
    };

    public SerializableDictionary<ResourceType, float> ConsumptionChange = new SerializableDictionary<ResourceType, float>()
    {
        {ResourceType.Sun, 20f},
        {ResourceType.Mineral, 20f},
        {ResourceType.Water, 20f}
    };

    public SerializableDictionary<ResourceType, float> ProductionChange = new SerializableDictionary<ResourceType, float>()
    {
        {ResourceType.Sun, 20f},
        {ResourceType.Mineral, 20f},
        {ResourceType.Water, 20f}
    };
}

[XmlRoot("dictionary")]
public class SerializableDictionary<TKey, TValue>: Dictionary<TKey, TValue>, IXmlSerializable
{
    #region IXmlSerializable Members
    public System.Xml.Schema.XmlSchema GetSchema()
    {
        return null;
    }

    public void ReadXml(System.Xml.XmlReader reader)
    {
        var keySerializer = new XmlSerializer(typeof(TKey));
        var valueSerializer = new XmlSerializer(typeof(TValue));

        var wasEmpty = reader.IsEmptyElement;
        reader.Read();

        if (wasEmpty)
            return;

        while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
        {
            reader.ReadStartElement("item");

            reader.ReadStartElement("key");
            var key = (TKey)keySerializer.Deserialize(reader);
            reader.ReadEndElement();

            reader.ReadStartElement("value");
            var value = (TValue)valueSerializer.Deserialize(reader);
            reader.ReadEndElement();

            Add(key, value);

            reader.ReadEndElement();
            reader.MoveToContent();
        }
        reader.ReadEndElement();
}

    public void WriteXml(System.Xml.XmlWriter writer)
    {
        var keySerializer = new XmlSerializer(typeof(TKey));
        var valueSerializer = new XmlSerializer(typeof(TValue));

        foreach (var key in Keys)
{
            writer.WriteStartElement("item");

            writer.WriteStartElement("key");
            keySerializer.Serialize(writer, key);
            writer.WriteEndElement();

            writer.WriteStartElement("value");
            var value = this[key];
            valueSerializer.Serialize(writer, value);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
    #endregion
}