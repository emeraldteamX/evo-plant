﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeatherController : MonoBehaviour
{
    public GameObject Cloud;

    public List<GameObject> Clouds { get; set; }

    public bool IsCloudiness { get; set; }

    public WeatherInfo WeatherInfo { get; set; }

    private void Awake()
    {
        Clouds = new List<GameObject>();
    }

    private void Update()
    {
        if (IsCloudiness)
        {
            // Probability of creating cloud each frame
            if (!(Random.value < 0.01f))
                return;
            var obj = Instantiate(Cloud);
            var height = 4f + Random.value;

            var cloud = obj.GetComponent<Cloud>();

            cloud.Destination = new Vector3(22f, height, 5f);
            cloud.StartPosition = new Vector3(-22f, height, 5f);
            cloud.ParentList = Clouds;

            Clouds.Add(obj);
        }

        WeatherInfo.NumberOfClouds = Clouds.Count;
    }
}
