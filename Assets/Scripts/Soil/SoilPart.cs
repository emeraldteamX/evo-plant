﻿using System.Collections.Generic;
using UnityEngine;

public class SoilPart : MonoBehaviour
{
    public float Wetness { get; set; }
    public float Toughness { get; set; }

    public float CurrentWater { get; set; }
    public float MaxWater { get; set; }

    public float CurrentMinerals { get; set; }
    public float MaxMinerals { get; set; }
    public void AddCollider(List<Vector2> polygon)
    {
        var collider = gameObject.AddComponent<PolygonCollider2D>();
        collider.points = polygon.ToArray();
    }
}
