﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ResourceSpawner : MonoBehaviour
{
    public event Func<ResourceType, bool> Spawning;

    #region Resource events and event handlers

    public event Action<ResourceType> ResourcePickUp;
    public event Action<ResourceType> ResourceDispose;

    protected void OnResourceDispose(ResourceType type)
    {
        if (ResourceDispose != null)
            ResourceDispose(type);
    }

    protected void OnResourcePickUp(ResourceType type)
    {
        if (ResourcePickUp != null)
            ResourcePickUp(type);
    }

    #endregion

    public GameObject ResourceToSpawn;
    public GameObject Spawner;

    private void Start()
    {
        switch (StringToEnum.Execute<ResourceType>(ResourceToSpawn.tag))
        {
            case ResourceType.Sun:
                InvokeRepeating("SpawnResource", Spawn.WaitTime, Spawn.SunTime);
                break;
            case ResourceType.Water:
                InvokeRepeating("SpawnResource", Spawn.WaitTime, Spawn.WaterTime);
                break;
            case ResourceType.Mineral:
                InvokeRepeating("SpawnResource", Spawn.WaitTime, Spawn.MineralTime);
                break;
        }
    }

    private void Update()
    {

    }

    /// <summary>
    /// Generate clickable resource on screen
    /// </summary>
    private void SpawnResource()
    {
        if (Spawning != null)
            if (!Spawning(StringToEnum.Execute<ResourceType>(ResourceToSpawn.tag)))
                return;

        var randomVector = new Vector3(Random.value, Random.value, 0);
        var resource =
            ((GameObject)
                Instantiate(ResourceToSpawn, Spawner.transform.position + randomVector, Quaternion.identity))
                .GetComponent<ResourceController>();
        resource.Dispose += OnResourceDispose;
        resource.PickUp += OnResourcePickUp;
    }
}
