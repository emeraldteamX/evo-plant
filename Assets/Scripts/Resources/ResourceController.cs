﻿using System;
using UnityEngine;

public class ResourceController : MonoBehaviour
{
    public event Action<ResourceType> Dispose;
    public event Action<ResourceType> PickUp;
    private ResourceType _resourceType;

    private void Start()
    {

        // Destroy by time
        Destroy(gameObject, Spawn.LifeTime);
    }

    public void Collect()
    {
        Destroy(gameObject);

        if (PickUp != null)
        {
            PickUp(_resourceType);
        }
    }

    private void OnDestroy()
    {
        if (Dispose != null)
            Dispose(_resourceType);
    }
}