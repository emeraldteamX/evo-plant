﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cloud : MonoBehaviour
{
    public Vector3 Destination { get; set; }

    public List<GameObject> ParentList { get; set; }

    public Vector3 StartPosition { get; set; }

    private void Start()
    {
        gameObject.transform.position = StartPosition;
    }

    void Update()
    {
        if (Destination == Vector3.zero)
            return;
        StartCoroutine(Move(0.01f));
    }

    private IEnumerator Move(float speed)
    {
        // Move cloud from StartPosition to Destination
        while (gameObject != null && gameObject.transform.position.x < Destination.x)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Destination, speed * Time.deltaTime);
            yield return null;
        }
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        // Remove cloud from list
        foreach (var element in ParentList)
            if (element.Equals(gameObject))
            {
                ParentList.Remove(element);
                return;
            }
    }
}
