﻿using System;
using UnityEngine;

public class PlaceHolderLogic : MonoBehaviour
{
    public ITreePart PlantParent { get; set; }
    public GameObject PlanParent { get; set; }

    private void Start()
    {
        Destroy(gameObject, 0.5f);
    }

    private void OnTriggerEnter2D(Component sender)
    {
        // If hit soil
        if (sender.tag == Enum.GetName(typeof(OtherType), OtherType.Soil))
            PlanParent.GetComponent<PlanLogic>().SoilCollision = true;
        // If hit plant parent
        if (sender.gameObject.Equals(PlantParent.GameObjectInstance))
            return;
        // If hit plant parent childs
        foreach (var child in PlantParent.Children)
        {
            if (sender.gameObject.Equals(child.GameObjectInstance))
                return;
        }
        // If hit other plant part
        foreach (var type in Enum.GetValues(typeof(TreeElementType)))
        {
            if (!sender.tag.Equals(type))
                continue;
            PlanParent.GetComponent<PlanLogic>().IsDestroy = true;
            return;
        }
    }

}
