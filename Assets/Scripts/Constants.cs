﻿#region Types

using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to convert string name to enums named constant, if this enums named constant are exist with current string name
/// </summary>
public static class StringToEnum
{
    /// <summary>
    /// Function to convert string enumValue to enum type TResult
    /// </summary>
    /// <typeparam name="TResult">Enum type</typeparam>
    /// <param name="enumValue">Name of enums named constant</param>
    /// <returns>Enums named constant</returns>
    public static TResult Execute<TResult>(string enumValue)
    {
        return (TResult)Enum.Parse(typeof(TResult), enumValue);
    }
}

/// <summary>
/// Types of resources
/// </summary>
public enum ResourceType
{
        Sun,
        Mineral,
        Water
}

/// <summary>
/// Types of tree parts
/// </summary>
public enum TreeElementType
{
    Body,
    Root,
    Leaf
}

public enum InsectType
{
    FlyingInsect,
    GroundInsect,
    UndergroundInsect
}

public enum ButtonType
{
    ButtonLeaf,
    ButtonBody,
    ButtonRoot,
    ButtonCancel,
    ButtonRemove,
    ButtonUpgrade
}

public enum OtherType
{
    PlaceHolder,
    Soil,
    Plan
}

#endregion

#region Resource data

/// <summary>
/// 
/// </summary>
public static class Spawn
{
    /// <summary>
    /// Time to spawn
    /// </summary>
    public const int SunTime = 5;
    public const int MineralTime = 12;
    public const int WaterTime = 9;
    /// <summary>
    /// Time before spawn start
    /// </summary>
    public const int WaitTime = 5;
    /// <summary>
    /// Time of life
    /// </summary>
    public const int LifeTime = 10;
    /// <summary>
    /// Max number of each clicable resource on screen at the same time
    /// </summary>
    public const int MaxNumber = 1;
}

/// <summary>
/// Default values of production and consumption
/// </summary>
public static class ProductionValues
{
    /// <summary>
    /// Default sun production per second
    /// </summary>
    public const int SunProduction = 2;
    /// <summary>
    /// Default mineral production per second
    /// </summary>
    public const int MineralProduction = 2;
    /// <summary>
    /// Default water production per second
    /// </summary>
    public const int WaterProduction = 2;
    /// <summary>
    /// Default sun consumption per second
    /// </summary>
    public const int SunConsumption = 1;
    /// <summary>
    /// Default mineral consumption per second
    /// </summary>
    public const int MineralConsumption = 1;
    /// <summary>
    /// Default water consumption per second
    /// </summary>
    public const int WaterConsumption = 1;
}

#endregion

#region Insects

/// <summary>
/// 
/// </summary>
public class Insects
{
    // Time to spawn
    public const int SpawnTime = 5;
    // Time before spawn start
    public const int WaitTime = 120;
    // Max damage of the insect
    public const int MaxDamage = 5;
}

#endregion

#region Capacity

public class Capacity
{
    public const int Sun = 100;
    public const int Mineral = 100;
    public const int Water = 100;
}

#endregion

#region Other constants

/// <summary>
/// Name of layer levels
/// </summary>
public static class LayerLevel
{
    public const int UI = 5;
    public const int Plant = 8;
    public const int Resource = 9;
    public const int Plan = 10;
}

public static class ElementsSpecifications
{
    public class Price
    {
        public float Suns { get; set; }
        public float Minerals { get; set; }
        public float Water { get; set; }
    }

    public class HingeJointParameters
    {
        public JointAngleLimits2D Limits { get; set; }
    }

    public class SpringJointParameters
    {
        public float Frequency { get; set; }
    }

    public class Element
    {
        public Vector3 Scale { get; set; }
        public Price Price { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public HingeJointParameters HingeInfo { get; set; }
        public SpringJointParameters SpringInfo { get; set; }

        public Element()
        {
            Scale = new Vector3();
            Price = new Price();
            HingeInfo = new HingeJointParameters();
            SpringInfo = new SpringJointParameters();
        }
    }

    public static Dictionary<TreeElementType, Element> Elements;

    public const float DecreaseScaleValue = 0.9f;
}

public static class DefaultButtonsPosition
{
    public static float X;
    public static float Y;
}

public static class Global
{
    public const string GameControllerName = "Game Controller";

    public const string BackgroundName = "Background";
}

public static class GlobalWeather
{
    public static float DayDuration { get; set; }
    public static float NightDuration { get; set; }
    public static float SolarStep { get; set; }
}

public enum Position
{
    None,
    Top,
    Bottom,
    LeftTop,
    LeftBottom,
    RightTop,
    RightBottom
};
public enum WeatherType
{
    None,
    Rain
};

public static class UpgradeComponents
{
    public const string Damaging = "Damaging";
}

public static class UpgradeBranches
{
    public const string LeafUpgrades = "Leafs";
    public const string BodyUpgrades = "Bodies";
    public const string RootUpgrades = "Roots";
    public const string FlowerUpgrades = "Flowers";
    public const string DefenceUpgrades = "Defence";
    public static string[] Branches =
    {
        LeafUpgrades,
        BodyUpgrades,
        RootUpgrades,
        FlowerUpgrades,
        DefenceUpgrades
    };
}

#endregion